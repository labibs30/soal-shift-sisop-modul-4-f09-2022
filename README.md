# soal-shift-sisop-modul-4-F09-2022

Anggota Kelompok :
* Gaudhiwaa Hendrasto	5025201066
* M Labib Alfaraby      5025201083

***
1. Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:

    **a.** Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13
    
    **Contoh : 
    “Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”**

    **b.** Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.

    **c.** Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.

    **d.** Setiap data yang terencode akan masuk dalam file “Wibu.log” 

    Contoh isi: 

    `RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat `

    `RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba`

    **e.** Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)

    **Note** : filesystem berfungsi normal layaknya linux pada umumnya, Mount source (root) filesystem adalah directory /home/[USER]/Documents, dalam penamaan file ‘/’ diabaikan, dan ekstensi tidak perlu di-encode

    ***
    ## Penyelesaian Soal 1
    ***
    Pertama-tama Kita perlu membuat fungsi untuk encode. Karena enkripsi atbash dan rot13 adalah enkripsi yang bisa berlaku 2 arah untuk encode dan decode, maka kita hanya perlu menggunakan 1 fungsi saja.
  ```c
  void atbashrot(char str[1000], char newStr[1000]) {
  if (!strcmp(str, ".") || !strcmp(str, "..")) {
    strcpy(newStr, str);
    return;
  };

  int i, flag = 0;
  i = 0;
  while (str[i] != '\0') {
    if (str[i] == '.') {
      flag = 1;
    }
    if (flag == 1) {
      newStr[i] = str[i];
      i++;
      continue;
    }

    if (!((str[i] >= 0 && str[i] < 65) || (str[i] > 90 && str[i] < 97) ||
          (str[i] > 122 && str[i] <= 127))) {
      if (str[i] >= 'A' && str[i] <= 'Z') {
        newStr[i] = 'Z' + 'A' - str[i]; //encode atbash cipher
      }
      if (str[i] >= 'a' && str[i] <= 'z') {
        newStr[i] = ((str[i]-'a'+13)%26)+'a'; //encode rot13
      }
    }

    if (((str[i] >= 0 && str[i] < 65) || (str[i] > 90 && str[i] < 97) ||
         (str[i] > 122 && str[i] <= 127))) {
      newStr[i] = str[i];
    }

    i++;
  }
  newStr[i] = '\0';
}
```
 Kemudian, dari fungsi di atas kita harus menentukan apakah folder tersebut harus kita encode dengan menggunakan strncmp(token, "Animeku_", 8) == 0.

Pengecekan di atas dilakukan pada fungsi xmp_readdir sehingga kita bisa mengubah nama yang ditampilkan pada file system fuse.

```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                       off_t offset, struct fuse_file_info *fi) {
  char fpath[1000];
  printf("\n🚀 READDIR\n");

  int hasToEncrypt = 0;
  char currPath[1000] = "", pathEnc[1000] = "";

  char *token = strtok(path, "/");
  while (token != NULL) {
    if (hasToEncrypt) {
      strcat(pathEnc, "/");
      strcat(pathEnc, token);
    } else if (!hasToEncrypt) {
      strcat(currPath, "/");
      strcat(currPath, token);
    }

    if (strncmp(token, "Animeku_", 8) == 0) {
      hasToEncrypt = 1;
    }
    else if (strncmp(token, "IAN_", 4) == 0) {
      hasToEncrypt = 2;
    }
    token = strtok(NULL, "/");
  }

  if (strcmp(path, "/") == 0) {
    path = dirpath;
    sprintf(fpath, "%s", path);
  } else {
    char pathDec[1000], pathDec2[1000];
    if (hasToEncrypt == 1) {
		if(isupper(pathEnc[0]))
			atbash(pathEnc, pathDec);
		else
			rot13Denc(pathEnc, pathDec);
      sprintf(fpath, "%s%s%s", dirpath, path, pathDec);
    } else if (hasToEncrypt == 2) {
	  vigen(pathEnc, pathDec, 1);
      sprintf(fpath, "%s%s%s", dirpath, path, pathDec);
    } else {
      sprintf(fpath, "%s%s", dirpath, path);
    }
  }

  int res = 0;

  DIR *dp;
  struct dirent *de;
  (void)offset;
  (void)fi;
  printf("temp send: %s\n", fpath);

  dp = opendir(fpath);

  if (dp == NULL) return -errno;

  while ((de = readdir(dp)) != NULL) {
    struct stat st;

    memset(&st, 0, sizeof(st));

    st.st_ino = de->d_ino;
    st.st_mode = de->d_type << 12;

    // Create variable for new name
    char temp[1000], temp2[1000];
	
    if (hasToEncrypt == 1) {
		if(isupper(de->d_name[0]))
			atbash(de->d_name, temp);
		else
			rot13Enc(de->d_name, temp);
    } else if (hasToEncrypt == 2) {
	  vigen(de->d_name, temp, 1);

    } else {
      strcpy(temp, de->d_name);
    }

    printf("temp send: %s\n", temp);

    res = (filler(buf, temp, &st, 0));

    if (res != 0) break;
  }

  closedir(dp);

  loglvlInfo("READDIR", path);

  return 0;
}
```

Pada fungsi ini kamu melakukan pengecekan dan melakukan pengecekan itu untuk sub directory" lainnya. Karena dibutuhkan untuk bisa bekerja secara rekursif.

Setelah kita mengetahui bahwa folder tersebut harus di encode, maka kita menyimpan pathEnc dan memberikan flag untuk menandakan.

Kemudian setelah itu kita membuka directory tersebut, dan mengencode de->d_name dengan algoritma atbash

```c
 if (strcmp(path, "/") == 0) {
    path = dirpath;
    sprintf(fpath, "%s", path);
  } else {
    char pathDec[1000], pathDec2[1000];
    if (hasToEncrypt == 1) {
      atbash(pathEnc, pathDec);
      sprintf(fpath, "%s%s%s", dirpath, path, pathDec);
    }
    // ...
    else {
      strcpy(temp, de->d_name);
    }
    // ...
```

Pada percabangan ini, jika path merupakan / maka bisa diskip, dan jika path memiliki awalan AtoZ, maka kita harus decode terlebih dahulu menggunakan fungsi atbash, hal ini karena setelah ini kita akan memasukkan path tersebut ke opendir, maka dibutuhkan path yang sebenarnya sehingga file bisa membaca directorynya.

Selain penambahan codingan pada xmp_readdir, dilakukan juga penyesuaian untuk mendecode pada saat melakukan xmp_getattr dan xmp_read karena pada saat fuse ingin menjalankan command ls -l atau cat, kita membutuhkan nama file yang asli sehingga bisa dicari dan ditampilkan. Maka ada code untuk mendecode namanya kembali.

```c
char *token = strtok(temp, "/");
  while (token != NULL) {
    if (hasToEncrypt) {
      strcat(pathEnc, "/");
      strcat(pathEnc, token);
    } else if (!hasToEncrypt) {
      strcat(currPath, "/");
      strcat(currPath, token);
    }

    if (strncmp(token, "Animeku_", 8) == 0) {
      hasToEncrypt = 1;
    } else if (strncmp(token, "IAN_", 4) == 0) {
      hasToEncrypt = 2;
    }

    token = strtok(NULL, "/");
  }

  char fpathToSend[2000], decrypted[1000], decrypted2[1000];
  if (hasToEncrypt == 1) {
    atbash(pathEnc, decrypted);
    sprintf(fpathToSend, "%s%s%s", dirpath, currPath, decrypted);
  } else if (hasToEncrypt == 2) {
    rot13Denc(pathEnc, decrypted);
    atbash(decrypted, decrypted2);
    sprintf(fpathToSend, "%s%s%s", dirpath, currPath, decrypted2);
  } else {
    sprintf(fpathToSend, "%s%s%s", dirpath, currPath, pathEnc);
  }

  printf("fpathToSend: %s\n", fpathToSend);
```

Screenshot :

Sebelum.
![hasil](./screenshot/1c.png)
Sesudah.
![hasil](./screenshot/1b.png)
Wibu.log.
![hasil](./screenshot/1a.png)

2. Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya untuk menambahkan fitur pada programnya dengan ketentuan sebagai berikut : 

    **a.** Jika suatu direktori dibuat dengan awalan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere).

    **b.** Jika suatu direktori di rename dengan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.

    **c.** Apabila nama direktori dihilangkan “IAN_”, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.

    **d.** Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada direktori “/home/[user]/hayolongapain_[kelompok].log”. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada filesystem.


    **e.** Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level INFO dan WARNING. Untuk log level WARNING, digunakan untuk mencatat syscall rmdir dan unlink. Sisanya, akan dicatat pada level INFO dengan format sebagai berikut : 

    [Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC]

    ***
    ## Penyelesaian Soal 2
    ***
    `nomor 2a. ` Dalam penyelesaian bagian a, kita akan membuat fungsi dengan nama vigen yang mempunyai tiga buah paramater, yakni string nama file / directory sebelum encript, lalu string yang di reference sebagai nama file / directory hasil dari encript, dan bool apakah akan dilakukan encript vigenere chiper. . Cara kerja cipher Vigenere ini yaitu dengan menjumlahkan notasi angka pada sebuah kata dengan notasi angka pada "key" dari cipher menurut alphabet. Jika huruf pada kata lebih banyak dari key, maka notasi angka penjumlahan huruf untuk huruf selanjutnya pada kata dilakukan dengan huruf pertama kembali pada key. Untuk decrypt cipher ini yaitu dengan melakukan hal yang berkebalikan dengan cara mengurangi huruf pada kata dengan huruf pada key.  Berikut implementasi fungsi vigen.

    ```c
     void vigen(char str[1000], char newStr[1000], bool encr) {
    if (!strcmp(str, ".") || !strcmp(str, "..")) {
        strcpy(newStr, str);
        return;
    };

    char key[] = "INNUGANTENG";
    int msgLen = strlen(str), keyLen = strlen(key), i, j;
    char newKey[msgLen];
    for (i = 0, j = 0; i < msgLen; ++i, ++j) {
        if (j == keyLen) j = 0;

        newKey[i] = key[j];
    }
    newKey[i] = '\0';

    int flag = 0;
    i = 0;
    while (str[i] != '\0') {
        char temp;
        if (str[i] == '.') {
        flag = 1;
        }
        if (flag == 1) {
        newStr[i] = str[i];
        i++;
        continue;
        }

        if (encr) {
        if (!((str[i] >= 0 && str[i] < 65) || (str[i] > 90 && str[i] < 97))) {
            if (str[i] >= 'A' && str[i] <= 'Z') {
            if (newKey[i] >= 'a' && newKey[i] <= 'z') {
                temp = newKey[i] - 'a' + 'A';
                newStr[i] = ((str[i] + temp) % 26) + 'A';
            } else {
                newStr[i] = ((str[i] + newKey[i]) % 26) + 'A';
            }
            }
            if (str[i] >= 'a' && str[i] <= 'z') {
            temp = str[i] - 'a' + 'A';
            if (newKey[i] >= 'a' && newKey[i] <= 'z') {
                char tempNK = newKey[i] - 'a' + 'A';
                newStr[i] = ((temp + tempNK) % 26) + 'A';
            } else {
                newStr[i] = ((temp + newKey[i]) % 26) + 'A';
            }
            newStr[i] = newStr[i] - 'A' + 'a';
            }
        }
        } else {
        if ((str[i] >= 0 && str[i] < 65) || (str[i] > 90 && str[i] < 97)) {
            if (!((str[i] >= 0 && str[i] < 65) || (str[i] > 90 && str[i] < 97))) {
            if (str[i] >= 'A' && str[i] <= 'Z') {
                if (newKey[i] >= 'a' && newKey[i] <= 'z') {
                temp = newKey[i] - 'a' + 'A';
                newStr[i] = (((str[i] + temp) + 26) % 26) + 'A';
                } else {
                newStr[i] = (((str[i] + newKey[i]) + 26) % 26) + 'A';
                }
            }
            if (str[i] >= 'a' && str[i] <= 'z') {
                temp = str[i] - 'a' + 'A';
                if (newKey[i] >= 'a' && newKey[i] <= 'z') {
                char tempNK = newKey[i] - 'a' + 'A';
                newStr[i] = (((temp + tempNK) + 26) % 26) + 'A';
                } else {
                newStr[i] = (((temp + newKey[i]) + 26) % 26) + 'A';
                }
                newStr[i] = newStr[i] - 'A' + 'a';
            }
            }
        }
        }

        i++;
    }
    newStr[i] = '\0';
    }


    ```

    `nomor 2b & 2c. ` Apabila sebuah direktori di rename dengan nama IAN_[nama] maka akan dilakukan encode seperti bagian a. sebaliknya apabila file diubah namanya dari IAN_[nama] ke sebuah nama lain maka akan dilakukan decode. Proses encode dan decode dikerjakan pada fungsi readdir, getattr, dan read
![hasil](./screenshot/2abc.png)

    `nomor 2d & 2e. ` Selanjutnya, kita akan membuat file log untuk memonitor kegiatana file system, terdiri dari dua jenis yaitu untuk level INFO dan WARNING yang mana telah dijelaskan dalam penjelasan soal. Ada beberapa fungsi yang terpisah untuk menjalankan setiap perintah write ke log, berikut fungsi - fungsinya.
    ```c
    void loglvlWarning(const char *log, const char *path) {
      FILE *fp;
      fp = fopen("/home/labib/hayolongapain_F09.log", "a");
      fputs("WARNING::", fp);
      char timestamp[1000];
      time_t t = time(NULL);
      struct tm tm = *localtime(&t);
      sprintf(timestamp, "%02d%02d%04d-%02d:%02d:%02d:", tm.tm_mday, tm.tm_mon + 1,
              tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec);
      fputs(timestamp, fp);
      fputs(log, fp);
      fputs("::", fp);
      fputs(path, fp);
      fputs("\n", fp);
      fclose(fp);
    }
    void loglvlInfo(const char *log, const char *path) {
      FILE *fp;
      fp = fopen("/home/labib/hayolongapain_F09.log", "a");
      fputs("INFO::", fp);
      char timestamp[1000];
      time_t t = time(NULL);
      struct tm tm = *localtime(&t);
      sprintf(timestamp, "%02d%02d%04d-%02d:%02d:%02d:", tm.tm_mday, tm.tm_mon + 1,
              tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec);
      fputs(timestamp, fp);
      fputs(log, fp);
      fputs("::", fp);
      fputs(path, fp);
      fputs("\n", fp);
      fclose(fp);
    }

    void loglvlInfo2(const char *log, const char *source, const char *destination) {
      FILE *fp;
      fp = fopen("/home/labib/hayolongapain_F09.log", "a");
      fputs("INFO::", fp);
      char timestamp[1000];
      time_t t = time(NULL);
      struct tm tm = *localtime(&t);
      sprintf(timestamp, "%02d%02d%04d-%02d:%02d:%02d:", tm.tm_mday, tm.tm_mon + 1,
              tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec);
      fputs(timestamp, fp);
      fputs(log, fp);
      fputs("::", fp);
      fputs(source, fp);
      fputs("::", fp);
      fputs(destination, fp);
      fputs("\n", fp);
      fclose(fp);
    }

    ```
  fungsi loglvlWarning untuk write level **WARNING** , lalu loglvlInfo untuk write level **INFO** selain rename, dan terakhir loglvlInfo2 untuk write level **INFO** untuk rename.
![hasil](./screenshot/2dc.png)
  ***

3. Ishaq adalah seseorang yang terkenal di kalangan anak informatika seluruh indonesia. Ia memiliki teman yang bernama innu dan anya, lalu ishaq bertemu dengan mereka dan melihat program yang mereka berdua kerjakan  sehingga ia tidak mau kalah dengan innu untuk membantu anya dengan menambah fitur yang ada pada programnya dengan ketentuan :.

    **a.** Jika suatu direktori dibuat dengan awalan “nam_do-saq_”, maka direktori tersebut akan menjadi sebuah direktori spesial

    **b.** Jika suatu direktori di-rename dengan memberi awalan “nam_do-saq_”,maka direktori tersebut akan menjadi sebuah direktori spesial.

    **c.** Apabila direktori yang terenkripsi di-rename dengan menghapus “nam_do-saq_” pada bagian awal nama folder maka direktori tersebut menjadi direktori normal.

    **d.** Direktori spesial adalah direktori yang mengembalikan enkripsi/encoding pada direktori “Animeku_” maupun “IAN_” namun masing masing aturan mereka tetap berjalan pada direktori di dalamnya (sifat recursive “Animeku_” dan “IAN_” tetap berjalan pada subdirektori).

    **e.** Pada direktori spesial semua nama file (tidak termasuk ekstensi) pada fuse akan berubah menjadi uppercase insensitive dan diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya.

    Contoh : jika pada direktori asli namanya adalah “isHaQ_KEreN.txt” maka pada fuse akan 
    menjadi “ISHAQ_KEREN.txt.1670”. 1670 berasal dari biner 11010000110

    ***
    ## Penyelesaian Soal 3
    ***
    
